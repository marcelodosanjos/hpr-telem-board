EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:stm32
LIBS:w_analog
LIBS:sprk_rfm69hcw
LIBS:gy-gps6mv2
LIBS:bme150
LIBS:hpr-telem-board-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 6 6
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L BME150 U?
U 1 1 58BF8328
P 5850 3950
F 0 "U?" H 6050 4250 60  0000 C CNN
F 1 "BME150" V 5850 3950 60  0000 C CNN
F 2 "" H 5850 3950 60  0001 C CNN
F 3 "" H 5850 3950 60  0001 C CNN
	1    5850 3950
	1    0    0    -1  
$EndComp
Text HLabel 6800 3800 2    60   Input ~ 0
VDD
$Comp
L GND #PWR?
U 1 1 58BF83FC
P 5350 3250
F 0 "#PWR?" H 5350 3000 50  0001 C CNN
F 1 "GND" H 5350 3100 50  0000 C CNN
F 2 "" H 5350 3250 50  0000 C CNN
F 3 "" H 5350 3250 50  0000 C CNN
	1    5350 3250
	-1   0    0    1   
$EndComp
Wire Wire Line
	6350 3800 6800 3800
Connection ~ 6450 3800
Wire Wire Line
	5350 3250 5350 3800
$Comp
L C C?
U 1 1 58BF846D
P 6450 3500
F 0 "C?" H 6475 3600 50  0000 L CNN
F 1 "100n" H 6475 3400 50  0000 L CNN
F 2 "" H 6488 3350 50  0000 C CNN
F 3 "" H 6450 3500 50  0000 C CNN
	1    6450 3500
	-1   0    0    1   
$EndComp
Wire Wire Line
	6450 3650 6450 3800
$Comp
L GND #PWR?
U 1 1 58BF84B3
P 6550 4550
F 0 "#PWR?" H 6550 4300 50  0001 C CNN
F 1 "GND" H 6550 4400 50  0000 C CNN
F 2 "" H 6550 4550 50  0000 C CNN
F 3 "" H 6550 4550 50  0000 C CNN
	1    6550 4550
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 3900 6550 3900
Wire Wire Line
	6550 3900 6550 4550
Wire Wire Line
	6350 4100 6550 4100
Connection ~ 6550 4100
Text HLabel 6800 4000 2    60   Input ~ 0
VDDIO
Wire Wire Line
	6350 4000 6800 4000
$Comp
L C C?
U 1 1 58BF84FA
P 6700 4300
F 0 "C?" H 6725 4400 50  0000 L CNN
F 1 "100n" H 6725 4200 50  0000 L CNN
F 2 "" H 6738 4150 50  0000 C CNN
F 3 "" H 6700 4300 50  0000 C CNN
	1    6700 4300
	1    0    0    -1  
$EndComp
Wire Wire Line
	6700 4150 6700 4000
Connection ~ 6700 4000
Wire Wire Line
	6700 4450 6550 4450
Connection ~ 6550 4450
Wire Wire Line
	5350 3900 5000 3900
Wire Wire Line
	5000 3900 5000 4450
Wire Wire Line
	6400 4450 6400 4000
Connection ~ 6400 4000
$Comp
L R R?
U 1 1 58BF858B
P 5350 4300
F 0 "R?" V 5430 4300 50  0000 C CNN
F 1 "4.7k" V 5350 4300 50  0000 C CNN
F 2 "" V 5280 4300 50  0000 C CNN
F 3 "" H 5350 4300 50  0000 C CNN
	1    5350 4300
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 4450 6400 4450
Connection ~ 5350 4450
Wire Wire Line
	5350 4150 5350 4100
Wire Wire Line
	5350 4100 4900 4100
Text HLabel 4900 4100 0    60   Input ~ 0
BME_SCK
$Comp
L R R?
U 1 1 58BF8674
P 5150 4300
F 0 "R?" V 5230 4300 50  0000 C CNN
F 1 "4.7k" V 5150 4300 50  0000 C CNN
F 2 "" V 5080 4300 50  0000 C CNN
F 3 "" H 5150 4300 50  0000 C CNN
	1    5150 4300
	1    0    0    -1  
$EndComp
Connection ~ 5150 4450
Wire Wire Line
	5150 4150 5150 4000
Wire Wire Line
	4900 4000 5350 4000
Connection ~ 5150 4000
Text HLabel 4900 4000 0    60   Input ~ 0
BME_SDA
Wire Wire Line
	6450 3350 5350 3350
Connection ~ 5350 3350
$EndSCHEMATC
