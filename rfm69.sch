EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:stm32
LIBS:w_analog
LIBS:sprk_rfm69hcw
LIBS:gy-gps6mv2
LIBS:bme150
LIBS:hpr-telem-board-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 6
Title "HPR Telemetry Module"
Date "2017-03-07"
Rev "0.1"
Comp "Libre Space Foundation"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L SPRK_RFM69HCW U?
U 1 1 58BF29E9
P 6000 3850
F 0 "U?" H 6550 4450 60  0000 C CNN
F 1 "SPRK_RFM69HCW" H 6000 3850 60  0000 C CNN
F 2 "" H 6000 3600 60  0001 C CNN
F 3 "" H 6000 3600 60  0001 C CNN
	1    6000 3850
	1    0    0    -1  
$EndComp
NoConn ~ 6850 3500
NoConn ~ 6850 3600
NoConn ~ 6850 3700
NoConn ~ 6850 3800
NoConn ~ 6850 3900
Wire Wire Line
	6850 4050 7000 4050
Wire Wire Line
	7000 4050 7000 4450
Wire Wire Line
	6850 4250 7000 4250
Connection ~ 7000 4250
Wire Wire Line
	6850 4150 7250 4150
$Comp
L GND #PWR?
U 1 1 58BF2A5C
P 7000 4450
F 0 "#PWR?" H 7000 4200 50  0001 C CNN
F 1 "GND" H 7000 4300 50  0000 C CNN
F 2 "" H 7000 4450 50  0000 C CNN
F 3 "" H 7000 4450 50  0000 C CNN
	1    7000 4450
	1    0    0    -1  
$EndComp
Text HLabel 7250 4150 2    60   Output ~ 0
ANT
Text HLabel 4950 3400 0    60   Input ~ 0
VDD
Wire Wire Line
	4950 3400 5150 3400
$Comp
L GND #PWR?
U 1 1 58BF2A80
P 5000 3500
F 0 "#PWR?" H 5000 3250 50  0001 C CNN
F 1 "GND" H 5000 3350 50  0000 C CNN
F 2 "" H 5000 3500 50  0000 C CNN
F 3 "" H 5000 3500 50  0000 C CNN
	1    5000 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 3500 5150 3500
Text HLabel 4800 3700 0    60   Output ~ 0
RF_MISO
Text HLabel 4800 3800 0    60   Input ~ 0
RF_MOSI
Text HLabel 4800 3900 0    60   Input ~ 0
RF_SCK
Text HLabel 4800 4000 0    60   BiDi ~ 0
RF_NSS
Text HLabel 7250 3400 2    60   Output ~ 0
RF_INT
Wire Wire Line
	7250 3400 6850 3400
Wire Wire Line
	4800 3700 5150 3700
Wire Wire Line
	4800 3800 5150 3800
Wire Wire Line
	4800 3900 5150 3900
Wire Wire Line
	4800 4000 5150 4000
NoConn ~ 5150 4250
$EndSCHEMATC
