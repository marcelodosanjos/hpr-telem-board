EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:stm32
LIBS:w_analog
LIBS:sprk_rfm69hcw
LIBS:gy-gps6mv2
LIBS:bme150
LIBS:hpr-telem-board-cache
EELAYER 25 0
EELAYER END
$Descr User 7874 4000
encoding utf-8
Sheet 5 6
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L GY-GPS6MV2 U?
U 1 1 58BF4442
P 1900 1500
F 0 "U?" H 1900 1500 60  0000 C CNN
F 1 "GY-GPS6MV2" H 1900 1350 60  0000 C CNN
F 2 "" H 1900 1500 60  0001 C CNN
F 3 "" H 1900 1500 60  0001 C CNN
	1    1900 1500
	1    0    0    -1  
$EndComp
Text HLabel 1600 2550 3    60   Input ~ 0
VCC
Text HLabel 2200 2550 3    60   Input ~ 0
GND
Text HLabel 1800 2550 3    60   Input ~ 0
GPS_RX
Text HLabel 2000 2550 3    60   Output ~ 0
GPS_TX
Wire Wire Line
	1600 2550 1600 2200
Wire Wire Line
	1800 2550 1800 2200
Wire Wire Line
	2000 2550 2000 2200
Wire Wire Line
	2200 2550 2200 2200
$EndSCHEMATC
