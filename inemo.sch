EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:stm32
LIBS:w_analog
LIBS:sprk_rfm69hcw
LIBS:gy-gps6mv2
LIBS:bme150
LIBS:hpr-telem-board-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 6
Title "HPR Telemetry Module"
Date "2017-03-07"
Rev "0.1"
Comp "Libre Space Foundation"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L LSM9DS0 U?
U 1 1 58BF1DE0
P 6000 3600
F 0 "U?" H 6350 4550 60  0000 C CNN
F 1 "LSM9DS0" H 6500 4450 60  0000 C CNN
F 2 "" H 6000 3750 60  0000 C CNN
F 3 "" H 6000 3750 60  0000 C CNN
	1    6000 3600
	1    0    0    -1  
$EndComp
Text HLabel 5800 2100 1    60   Input ~ 0
VDD
Wire Wire Line
	5800 2100 5800 2700
Wire Wire Line
	5250 2350 6000 2350
Wire Wire Line
	5900 2350 5900 2700
Connection ~ 5800 2350
Wire Wire Line
	6000 2350 6000 2700
Connection ~ 5900 2350
$Comp
L C C?
U 1 1 58BF1E98
P 5550 2200
F 0 "C?" H 5575 2300 50  0000 L CNN
F 1 "10u" H 5575 2100 50  0000 L CNN
F 2 "" H 5588 2050 50  0000 C CNN
F 3 "" H 5550 2200 50  0000 C CNN
	1    5550 2200
	1    0    0    -1  
$EndComp
$Comp
L C C?
U 1 1 58BF1F1D
P 5250 2200
F 0 "C?" H 5275 2300 50  0000 L CNN
F 1 "100n" H 5275 2100 50  0000 L CNN
F 2 "" H 5288 2050 50  0000 C CNN
F 3 "" H 5250 2200 50  0000 C CNN
	1    5250 2200
	1    0    0    -1  
$EndComp
Connection ~ 5550 2350
Wire Wire Line
	4950 2050 5550 2050
Wire Wire Line
	4950 2050 4950 2250
Connection ~ 5250 2050
$Comp
L GND #PWR?
U 1 1 58BF1FE1
P 4950 2250
F 0 "#PWR?" H 4950 2000 50  0001 C CNN
F 1 "GND" H 4950 2100 50  0000 C CNN
F 2 "" H 4950 2250 50  0000 C CNN
F 3 "" H 4950 2250 50  0000 C CNN
	1    4950 2250
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 58BF2025
P 5850 4750
F 0 "#PWR?" H 5850 4500 50  0001 C CNN
F 1 "GND" H 5850 4600 50  0000 C CNN
F 2 "" H 5850 4750 50  0000 C CNN
F 3 "" H 5850 4750 50  0000 C CNN
	1    5850 4750
	1    0    0    -1  
$EndComp
Wire Wire Line
	5850 4500 5850 4750
Wire Wire Line
	5750 4500 5750 4600
Wire Wire Line
	5750 4600 6250 4600
Connection ~ 5850 4600
$Comp
L C C?
U 1 1 58BF2109
P 6950 3650
F 0 "C?" H 6975 3750 50  0000 L CNN
F 1 "0.22u" H 6975 3550 50  0000 L CNN
F 2 "" H 6988 3500 50  0000 C CNN
F 3 "" H 6950 3650 50  0000 C CNN
	1    6950 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	6700 3500 6950 3500
Wire Wire Line
	6700 3700 6800 3700
Wire Wire Line
	6800 3700 6800 3800
Wire Wire Line
	6800 3800 6950 3800
$Comp
L GND #PWR?
U 1 1 58BF215E
P 6950 4500
F 0 "#PWR?" H 6950 4250 50  0001 C CNN
F 1 "GND" H 6950 4350 50  0000 C CNN
F 2 "" H 6950 4500 50  0000 C CNN
F 3 "" H 6950 4500 50  0000 C CNN
	1    6950 4500
	1    0    0    -1  
$EndComp
$Comp
L C C?
U 1 1 58BF217A
P 6950 4350
F 0 "C?" H 6975 4450 50  0000 L CNN
F 1 "4.7u" H 6975 4250 50  0000 L CNN
F 2 "" H 6988 4200 50  0000 C CNN
F 3 "" H 6950 4350 50  0000 C CNN
	1    6950 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	6700 4200 6950 4200
NoConn ~ 6700 3000
NoConn ~ 5300 3700
NoConn ~ 5300 3800
Wire Wire Line
	6050 4600 6050 4500
Wire Wire Line
	6150 4600 6150 4500
Connection ~ 6050 4600
Wire Wire Line
	6250 4600 6250 4500
Connection ~ 6150 4600
NoConn ~ 5300 3200
NoConn ~ 5300 3300
Wire Wire Line
	6200 2700 6200 2600
Wire Wire Line
	6200 2600 5100 2600
Wire Wire Line
	5100 2600 5100 3600
Wire Wire Line
	5100 3000 5300 3000
Wire Wire Line
	5100 3600 5300 3600
Connection ~ 5100 3000
Wire Wire Line
	5300 3100 5100 3100
Connection ~ 5100 3100
NoConn ~ 5300 3400
NoConn ~ 5300 3900
Text HLabel 4900 4100 0    60   Input ~ 0
INEMO_SCL
Text HLabel 4900 4200 0    60   Input ~ 0
INEMO_SDA
Wire Wire Line
	4900 4100 5300 4100
Wire Wire Line
	4900 4200 5300 4200
$EndSCHEMATC
